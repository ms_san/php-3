<?php

function ubah_huruf($string){
  $aplha = "abcdefghijklmnopqrstuvwxyz";
    for ($i=1; $i<=strlen($string); $i++){
      for ($i = 0; $i < strlen($string); $i++){
        $char = $string[$i];
        $position = strpos($aplha, $char);
        echo substr($aplha, $position+1, 1);
      }
    }           
}

echo "wow // ";
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo 'developer // ';
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo 'laravel // ';
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo 'keren // ';
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo 'semangat // ';
echo ubah_huruf('semangat'); // tfnbohbu

?>